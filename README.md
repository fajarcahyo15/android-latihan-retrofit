<h2> Latihan Membuat Request Menggunakan Retrofit Android </h2>

<ul>
    <li>Keterangan:
        <ul>
            <li>SDK 26.0.0</li>
        </ul>
    </li>
    <li>Fitur:
        <ul>
            <li>Menampilkan Data Wifi Kota Bandung</li>
            <li>Menambah Data</li>
            <li>Edit Data</li>
            <li>Hapus Data</li>
            <li>Melihat Detail Wifi</li>
            <li>Menampilkan Lokasi dan Jalan Menuju Titik Wifi yang Dipilih</li>
        </ul>
    </li>
    <li>Depedency:
        <ul>
            <li>Retrofit</li>
            <li>RxJava</li>
            <li>ButterKnife</li>
            <li>Location Sevice</li>
            <li>EasyPermission</li>
        </ul>
    </li>
    <li>Sumber Data: <a href="http://latihanwebservice.comeze.com/"><i>latihanwebservice.comeze.com</i></a></li>
</ul>
