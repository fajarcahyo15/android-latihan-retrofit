package com.latihan.latihanrertofit.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.latihan.latihanrertofit.R;
import com.latihan.latihanrertofit.model.Wifi;

import java.util.List;

/**
 * Created by fajar on 09/07/17.
 */

public class WifiAdapter extends ArrayAdapter<Wifi> {


    public WifiAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public WifiAdapter(Context context, int resource, List<Wifi> wifi){
        super(context,resource,wifi);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View v = convertView;

        if(v == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.wifi_list,null);

        }

        Wifi wifi = getItem(position);

        if(wifi!=null){
            TextView tv_nama = v.findViewById(R.id.tv_nama);
            TextView tv_keterangan = v.findViewById(R.id.tv_keterangan);
            TextView tv_lokasi = v.findViewById(R.id.tv_lokasi);

            tv_nama.setText(wifi.getNama());
            tv_keterangan.setText(wifi.getFasilitas());
            tv_lokasi.setText(wifi.getNama_lokasi());
        }

        return v;
    }
}
