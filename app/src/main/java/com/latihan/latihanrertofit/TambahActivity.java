package com.latihan.latihanrertofit;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.latihan.latihanrertofit.model.Hasil;
import com.latihan.latihanrertofit.rest.ApiClient;
import com.latihan.latihanrertofit.rest.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pub.devrel.easypermissions.EasyPermissions;

public class TambahActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.txt_nama)
    TextView txt_nama;

    @BindView(R.id.txt_latitude)
    TextView txt_latitude;

    @BindView(R.id.txt_longitude)
    TextView txt_longitude;

    @BindView(R.id.txt_lokasi)
    TextView txt_lokasi;

    @BindView(R.id.txt_keterangan)
    TextView txt_keterangan;

    @BindView(R.id.btn_simpan)
    Button btn_simpan;

    private static final int REQUEST_LOCATION = 100;
    private static final int UPDATE_INTERVAL = 500;
    private static final int FASTEST_INTERVAL = 100;
    private Location location;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah);
        ButterKnife.bind(this);
        setToolbar();
        txt_latitude.setEnabled(false);
        txt_longitude.setEnabled(false);
        setupGoogleAPI();

    }

    private void setToolbar() {
        getSupportActionBar().setTitle("Wifi Bandung Juara");
        getSupportActionBar().setSubtitle("Tambah Data");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void setupGoogleAPI() {
        // initialize Google API Client
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        googleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }


    private void getLokasi() {

        int permissionGPS = ContextCompat.checkSelfPermission(TambahActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if ((permissionGPS != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(TambahActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }

        if ((permissionGPS == PackageManager.PERMISSION_GRANTED)) {

            location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null){
                txt_latitude.setText(String.valueOf(location.getLatitude()));
                txt_longitude.setText(String.valueOf(location.getLongitude()));
            }else{
                Toast.makeText(this," Lokasi Null", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void pesanSukses(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pesan");
        builder.setMessage(message);
        builder.setNeutralButton("OK",
                (dialogInterface, i) -> {
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                });

        builder.create();
        builder.show();
    }

    @OnClick(R.id.btn_simpan)
    void simpan() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Upload Data");
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        String nama = txt_nama.getText().toString();
        String latitude = txt_latitude.getText().toString();
        String longitude = txt_longitude.getText().toString();
        String keterangan = txt_keterangan.getText().toString();
        String lokasi = txt_lokasi.getText().toString();

        if (nama.isEmpty() || latitude.isEmpty() || longitude.isEmpty() || keterangan.isEmpty() || lokasi.isEmpty()) {
            progressDialog.dismiss();
            Toast.makeText(this, "Data yang anda masukkan tidak valid", Toast.LENGTH_SHORT).show();
        } else {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Observable<Hasil> responseCall = apiService.simpan(nama, latitude, longitude, keterangan, lokasi);

            responseCall.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            hasil -> {
                                if (hasil.getValue().equals("1")) {
                                    pesanSukses(hasil.getMessage());
                                } else if (hasil.getValue().equals("0")) {
                                    Toast.makeText(TambahActivity.this, hasil.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            },

                            error -> {
                                Toast.makeText(TambahActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                            },

                            () -> {
                                progressDialog.dismiss();
                            }
                    );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLokasi();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this," Tidak ada koneksi", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this," Can't Connected to Google Location API", Toast.LENGTH_LONG).show();
    }

//    Lifcycle
    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

}
