package com.latihan.latihanrertofit;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.latihan.latihanrertofit.model.Wifi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailActivity extends AppCompatActivity {


    @BindView(R.id.wv_gmaps)
    WebView wv_gmaps;

    @BindView(R.id.tv_lokasi)
    TextView tv_lokasi;

    @BindView(R.id.tv_keterangan)
    TextView tv_keterangan;

    @BindView(R.id.btn_navigate)
    FloatingActionButton btn_navigate;

    private Wifi wifi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        wifi = getIntent().getParcelableExtra("dataWifi");
        ButterKnife.bind(this);
        setToolbar();

//        wv_gmaps.setOnTouchListener((view, motionEvent) -> true);
        wv_gmaps.setHorizontalScrollBarEnabled(false);
        wv_gmaps.setVerticalScrollBarEnabled(false);
        wv_gmaps.setWebViewClient(new WebViewClient());
        wv_gmaps.getSettings().setJavaScriptEnabled(true);
//        wv_gmaps.loadUrl("https://www.google.co.id/maps/@"+wifi.getLatitude()+","+wifi.getLongitude()+",15z?hl=id&style=feature:all|element:labels|visibility:off");
        wv_gmaps.loadUrl("https://maps.google.com/maps?q="+wifi.getLatitude()+","+wifi.getLongitude()+"&style=feature:all|element:labels|visibility:off");
        tv_lokasi.setText(wifi.getNama_lokasi());
        tv_keterangan.setText("Fasilitas : \n"+wifi.getFasilitas());

    }

    private void setToolbar() {
        getSupportActionBar().setTitle("Wifi Bandung Juara");
        getSupportActionBar().setSubtitle(wifi.getNama());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @OnClick(R.id.btn_navigate)
    void navigate(){
        Toast.makeText(DetailActivity.this,"TEST",Toast.LENGTH_SHORT).show();
    }




}
