package com.latihan.latihanrertofit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fajar on 09/07/17.
 */

public class Hasil {

    String value;
    String message;

    @SerializedName("lokasi")
    List<Wifi> wifiList;

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public List<Wifi> getWifiList() {
        return wifiList;
    }
}
