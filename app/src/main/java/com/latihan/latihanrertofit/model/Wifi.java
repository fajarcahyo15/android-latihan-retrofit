package com.latihan.latihanrertofit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fajar on 09/07/17.
 */

public class Wifi implements Parcelable {

    @SerializedName("no")
    String no;

    @SerializedName("nama")
    String nama;

    @SerializedName("latitude")
    String latitude;

    @SerializedName("longitude")
    String longitude;

    @SerializedName("fasilitas")
    String fasilitas;

    @SerializedName("nama_lokasi")
    String nama_lokasi;

    protected Wifi(Parcel in) {
        no = in.readString();
        nama = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        fasilitas = in.readString();
        nama_lokasi = in.readString();
    }

    public static final Creator<Wifi> CREATOR = new Creator<Wifi>() {
        @Override
        public Wifi createFromParcel(Parcel in) {
            return new Wifi(in);
        }

        @Override
        public Wifi[] newArray(int size) {
            return new Wifi[size];
        }
    };

    public String getNo() {
        return no;
    }

    public String getNama() {
        return nama;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getFasilitas() {
        return fasilitas;
    }

    public String getNama_lokasi() {
        return nama_lokasi;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(no);
        parcel.writeString(nama);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(fasilitas);
        parcel.writeString(nama_lokasi);
    }
}
