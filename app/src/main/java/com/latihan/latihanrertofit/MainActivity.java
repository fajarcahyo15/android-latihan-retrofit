package com.latihan.latihanrertofit;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.latihan.latihanrertofit.adapter.WifiAdapter;
import com.latihan.latihanrertofit.model.Hasil;
import com.latihan.latihanrertofit.model.Wifi;
import com.latihan.latihanrertofit.rest.ApiClient;
import com.latihan.latihanrertofit.rest.ApiInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.lv_wifi)
    ListView lv_wifi;

    @BindView(R.id.refreshWifi)
    SwipeRefreshLayout refreshWifi;

    private List<Wifi> wifiList;


    private static final int TAMBAH_DATA = 1;
    private static final int LIHAT_DETAIL = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setToolbar();

        progressLoadData();

        lv_wifi.addHeaderView(new View(this));
        lv_wifi.addFooterView(new View(this));

        loadData();

        refreshWifi.setOnRefreshListener(() -> {
            loadData();
            refreshWifi.setRefreshing(false);
        });

    }


    //Call Action ===========================================================

    private void progressLoadData(){
        refreshWifi.setRefreshing(true);
    }

    private void progressHapus(){
        refreshWifi.setRefreshing(true);
    }

    private void loadData(){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Observable<Hasil> responseCall = apiService.getWifi();

        responseCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(

                        hasil -> {
                            wifiList = hasil.getWifiList();
                            WifiAdapter wifiAdapter = new WifiAdapter(MainActivity.this, R.layout.wifi_list,wifiList);
                            wifiAdapter.notifyDataSetChanged();
                            lv_wifi.setAdapter(wifiAdapter);
                        },

                        error -> {
                            Toast.makeText(MainActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                            refreshWifi.setRefreshing(false);
                        },

                        () -> {
                            refreshWifi.setRefreshing(false);
                        }
                );
    }

    private void setToolbar() {
        getSupportActionBar().setTitle(" Wifi Bandung Juara");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_toolbar_icon);

    }

    private void tambah_data(){
        Intent intent =  new Intent(MainActivity.this,TambahActivity.class);
        startActivityForResult(intent,TAMBAH_DATA);
    }

    private void hapus(final int position){
        final AlertDialog.Builder konfHapus = new AlertDialog.Builder(MainActivity.this);
        konfHapus.setTitle("Konfirmasi");
        konfHapus.setMessage("Apakah anda yakin ingin menghapus "+wifiList.get(position-1).getNama());
        konfHapus.setCancelable(false);
        konfHapus.setPositiveButton("OK", (dialogInterface, i) -> aksi_hapus(wifiList.get(position-1).getNo()));
        konfHapus.setNegativeButton("Batal", (dialogInterface, i) -> dialogInterface.dismiss());
        konfHapus.create();
        konfHapus.show();
    }

    private void aksi_hapus(String id){
        progressHapus();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Observable<Hasil> responseCall = apiService.hapus("1",id);

        responseCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(

                    hasil -> {
                        if(hasil.getValue().equals("1")){
                            refreshWifi.setRefreshing(true);
                            loadData();

                        }else if(hasil.getValue().equals("0")){
                            Toast.makeText(MainActivity.this,hasil.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    },

                    error -> {
                        Toast.makeText(MainActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
                    },

                    () -> {
                        Toast.makeText(MainActivity.this,"Data sukses dihapus",Toast.LENGTH_SHORT).show();
                    }
                );
    }

    //Component Action ========================================================

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add :
                tambah_data();
                break;
            default :
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnItemLongClick(R.id.lv_wifi)
    boolean listLongClick(int position) {
        String[] aksi = {"Edit", "Hapus"};

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Aksi")
                .setItems(aksi, (dialogInterface, i) -> {
                    switch (i) {
                        case 0 :

                            break;
                        case 1 :
                            hapus(position);
                            break;
                        default:
                            break;
                    }
                });
        dialog.create();
        dialog.show();
        return false;
    }

    @OnItemClick(R.id.lv_wifi)
    void listClick(int position) {
        Intent intent = new Intent(MainActivity.this,DetailActivity.class);
        intent.putExtra("dataWifi",wifiList.get(position-1));
        startActivityForResult(intent,LIHAT_DETAIL);

    }

    //LifeCycle Action ========================================================


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==TAMBAH_DATA){
            switch (resultCode){
                case RESULT_OK :
                    refreshWifi.setRefreshing(true);
                    loadData();
                    break;
                case RESULT_CANCELED :
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }


}
