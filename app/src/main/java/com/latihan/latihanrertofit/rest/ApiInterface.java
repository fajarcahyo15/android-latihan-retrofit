package com.latihan.latihanrertofit.rest;

import com.latihan.latihanrertofit.model.Hasil;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by fajar on 09/07/17.
 */

public interface ApiInterface {

    @GET("api/lokasi")
    Observable<Hasil> getWifi();

    @FormUrlEncoded
    @POST("wifi/tambah")
    Observable<Hasil> simpan(@Field("nama") String nama,
                       @Field("latitude") String latitude,
                       @Field("longitude") String longitude,
                       @Field("detail") String keterangan,
                       @Field("lokasi") String lokasi);

    @FormUrlEncoded
    @POST("wifi/edit")
    Observable<Hasil> edit(@Field("no") String no,
                             @Field("nama") String nama,
                             @Field("latitude") String latitude,
                             @Field("longitude") String longitude,
                             @Field("detail") String keterangan,
                             @Field("lokasi") String lokasi);

    @FormUrlEncoded
    @POST("wifi/hapus")
    Observable<Hasil> hapus(@Field("token") String token,
                       @Field("id") String id);



}
